import { useState } from 'react';
import './AddTask.scss';

const AddTask = (props) => {
    const [openForm, setOpenForm] = useState(false);
    const [task, setTask] = useState('');

    const onCreateTask = (ev) => {
        ev.preventDefault();
        if (task) {
            props.newTask(task);
            setTask('');
        }
    };

    const handleOpenForm = () => {
        setOpenForm(!openForm);
        setTask('');
    };

    const handleInput = (ev) => {
        const { value } = ev.target;
        setTask(value);
    };

    return (
        <div className="add-task">
            {!openForm && (
                <div clasName="add-task__text" onClick={handleOpenForm}>
                    <span className="add-task__icon">+</span>
                    Añadir Tarea
                </div>
            )}
            {openForm && (
                <div>
                    <form onSubmit={onCreateTask}>
                        <input
                            type="text"
                            onChange={handleInput}
                            value={task}
                        />
                        <div>
                            <button type="submit">Add Task</button>
                            <button onClick={handleOpenForm}>Cancel</button>
                        </div>
                    </form>
                </div>
            )}
        </div>
    );
};

export default AddTask;

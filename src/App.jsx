import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import {v4 as uuidv4} from 'uuid';
import { Navbar, MainList, Completed } from "./components";
import "./App.scss";

const INITIAL_TODOS = [
    {
        id: "ed8f7f49-1641-46b1-a838-b77804b2071a",
        title: "Hacer la comida",
    },
    {
        id: "c7353e17-705c-4d0d-a347-3474475627cb",
        title: "Terminar el proyecto de Node",
    },
    {
        id: "b2e48a01-9797-4e6b-ad0d-fa8785e53c07",
        title: "Aprender React",
    },
    {
        id: "82b62750-7ec5-4d6b-9c87-2bb8637537f6",
        title: "Echar la siesta",
    },
];

const App = () => {
    const [todos, setTodos] = useState(INITIAL_TODOS);
    const [completed, setCompleted] = useState([]);
    const [navbarInputValue, setNavbarInputValue] = useState(' ');

    const completeTodo = (todoId) => {
        const todo = todos.find((t) => t.id === todoId);
        setCompleted([...completed, todo]);
        const newTodos = todos.filter((t) => t.id !== todoId);
        setTodos(newTodos);
    };

    const uncopleteTodo = (todoId) => {
        const todo = completed.find((t) => t.id === todoId);
        setTodos([...todos, todo]);
        const newTodos = completed.filter((t) => t.id !== todoId);
        setCompleted(newTodos);
    };

    const newTask = (task) => {
        const taskToAdd = {
            title: task,
            id: uuidv4(),
        };
        setTodos([...todos, taskToAdd]);
    }

    const handleNavbarInput = (value) => {
        setNavbarInputValue(value.toLowerCase());
    };

    const filterTodos = () => {
        if(navbarInputValue){
            return todos.filter(todo => todo.title.toLowerCase().includes(navbarInputValue));
        }else{
            return todos;
        }
    };

    return (
        <Router>
            <div className="app">
                <Navbar handleNavbarInput={handleNavbarInput} navbarInputValue={navbarInputValue} />
                <Switch>
                    <Route
                        exact
                        path="/"
                        component={(props) => (
                            <MainList
                                {...props}
                                todos={filterTodos()}
                                handleTodo={completeTodo}
                                newTask={newTask}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/completed"
                        component={(props) => (
                            <Completed
                                {...props}
                                completed={completed}
                                handleTodo={uncopleteTodo}
                            />
                        )}
                    />
                </Switch>
            </div>
        </Router>
    );
};

export default App;
